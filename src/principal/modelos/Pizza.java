package principal.modelos;

import java.util.ArrayList;
import java.util.List;

public class Pizza {

	private String nome;
	//private Ingrediente[] ingredientes;
	private List<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
	
	public Pizza() {
		// ingredientes[3];
		// ingredientes.get(3);
	}
	
	public Pizza(String nome) {
		this.nome = nome;
	}
	
	
	public void adicionar(Ingrediente ingrediente) {
		//this.ingredientes[0] = ingrediente; errado
		this.ingredientes.add(ingrediente);
	}
	
	public void remover(Ingrediente ingrediente) {
		this.ingredientes.remove(ingrediente);
	}
	
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}
	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
}
