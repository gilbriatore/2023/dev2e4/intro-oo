package principal.modelos;

public class Bike {
	
	//Encapsulamento
	private String modelo;
	private int velocidade;
	private int pedaladasPorMinuto;
	private int marcha;	
	private Quadro quadro;	
	private Roda rodaDianteira;
	private Roda rodaTraseira;
	
	//Construtores
	public Bike() {
		
	}
	
	public Bike(String modelo, int velocidade, int pedaladas, int marcha) {
		this.modelo = modelo;
		this.velocidade = velocidade;
		this.pedaladasPorMinuto = pedaladas;
		this.marcha = marcha;
	}
	
	public Bike(String modelo, int velocidade, int pedaladas, int marcha,
			    Quadro quadro, Roda rodaDianteira, Roda rodaTraseira) {
		this.modelo = modelo;
		this.velocidade = velocidade;
		this.pedaladasPorMinuto = pedaladas;
		this.marcha = marcha;
		this.quadro = quadro;
		this.rodaDianteira = rodaDianteira;
		this.rodaTraseira = rodaTraseira;
	}
	
	
	//Getters (pegar)
	public String getModelo() {
		return modelo;
	}
	
	public int getVelocidade() {
		return velocidade;
	}
	
	public int getPedaladasPorMinuto() {
		return pedaladasPorMinuto;
	}
	
	public int getMarcha() {
		return marcha;
	}
	
	//Setters (definir)
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public void setVelocidade(int velocidade) {
		if (velocidade >= 0) {
			this.velocidade = velocidade;
		}
	}
	
	public void setPedaladasPorMinuto(int pedaladas) {
		this.pedaladasPorMinuto = pedaladas;
	}
	
	public void setMarcha(int marcha) {
		this.marcha = marcha;
	}

	public Quadro getQuadro() {
		return quadro;
	}

	public void setQuadroDaBike(Quadro quadroDaBike) {
		this.quadro = quadroDaBike;
	}

	public Roda getRodaDianteira() {
		return rodaDianteira;
	}

	public void setRodaDianteira(Roda rodaDianteira) {
		this.rodaDianteira = rodaDianteira;
	}

	public Roda getRodaTraseira() {
		return rodaTraseira;
	}

	public void setRodaTraseira(Roda rodaTraseira) {
		this.rodaTraseira = rodaTraseira;
	}
	
	

}
