package principal.modelos;

public class BikeDeTrilha extends Bike {

	public BikeDeTrilha(String modelo, int velocidade, 
			int pedaladas, int marcha, Quadro quadro, Roda rodaDianteira,
			Roda rodaTraseira) {
		
		super(modelo, velocidade, pedaladas, marcha, 
				quadro, rodaDianteira, rodaTraseira);
	}
}
