package principal;

import principal.modelos.*;

public class Programa {
	
	public static void main(String[] args) {
		
		//CRIAÇÃO DA BIKE	
//		Bike bikeNormal = new Bike();
//		BikeDeTrilha bikeTrilha = new BikeDeTrilha();
//		bikeTrilha.setModelo("Monark");
		
		
		
		
		
		
	//	Quadro quadroDaBike = new Quadro("Vermelha", "Alumínio", "Grande");		
	//	Roda rodaDianteiraDaBike = new Roda("20", "Alumínio", "Alumínio");		
	//	Roda rodaTraseiraDaBike = new Roda("20", "Alumínio", "Alumínio");
	//	Bike minhaBike = new Bike("Caloi", 0, 1, 0, 
	//			quadroDaBike, rodaDianteiraDaBike, rodaTraseiraDaBike);
		
		BikeDeTrilha minhaBike = new BikeDeTrilha("Caloi", 0, 1, 0, 
				new Quadro("Vermelha", "Alumínio", "Grande"), 
				new Roda("20", "Alumínio", "Alumínio"), 
				new Roda("20", "Alumínio", "Alumínio"));
		
		//A bike foi pintada de amarela
		minhaBike.getQuadro().setCor("Amarela");
		
		//IMPRESSÃO DE DADOS DA BIKE
		System.out.println("Modelo: " + minhaBike.getModelo());
		System.out.println("Velocidade: " + minhaBike.getVelocidade());
		System.out.println("Marcha: " + minhaBike.getMarcha());
		System.out.println("Pedaladas: " + minhaBike.getPedaladasPorMinuto());
		
		System.out.println("Cor do quadro: " + minhaBike.getQuadro().getCor());
		System.out.println("Tamanho do quadro: " + minhaBike.getQuadro().getTamanho());
		System.out.println("Material do quadro: " + minhaBike.getQuadro().getMaterial());
		
		System.out.println("Cor da roda dianteira: " + minhaBike.getRodaDianteira().getCor());
		System.out.println("Aro da roda dianteira: " + minhaBike.getRodaDianteira().getAro());
		System.out.println("Material da roda dianteira: " + minhaBike.getRodaDianteira().getMaterial());
		
		
		System.out.println("Cor da roda traseira: " + minhaBike.getRodaTraseira().getCor());
		System.out.println("Aro da roda traseira: " + minhaBike.getRodaTraseira().getAro());
		System.out.println("Material da roda traseira: " + minhaBike.getRodaTraseira().getMaterial());
		
		
		//... daqui 20min		
		//minhaBike.setVelocidade(20);
		
		//Pode isso?
		//minhaBike.setVelocidade(-20);
			
		
	}
	
	
//	public static void main(String[] args) {
//		
//		Ingrediente molho = new Ingrediente("Molho", "Tomate");
//		Ingrediente queijo = new Ingrediente("Queijo", "Parmesão"); 
//		
////		List<Ingrediente> ingredientes = new ArrayList<Ingrediente>();
////		ingredientes.add(molho);
////		ingredientes.add(queijo);
//		
//		Pizza pizzaDeQueijo = new Pizza("Queijo");
////		pizzaDeQueijo.setIngredientes(ingredientes); 		
//		pizzaDeQueijo.adicionar(molho);
//		pizzaDeQueijo.adicionar(queijo);
//		
//		//IMPRIMIR INGREDIENTES DA PIZZA
//		
//		System.out.println("Pizza: " + pizzaDeQueijo.getNome());
//		
//		//FOR BÁSICO
////		for(int i = 0; i < pizzaDeQueijo.getIngredientes().size(); i++) {
////			Ingrediente ingrediente = pizzaDeQueijo.getIngredientes().get(i);
////			System.out.println("Nome ingrediente: " + ingrediente.getNome());
////			System.out.println("Descrição ingrediente: " + ingrediente.getDescricao());
////		}
//		
//		//FOR AVANÇADO
//		for(Ingrediente ingrediente : pizzaDeQueijo.getIngredientes()) {
//			System.out.println("Nome ingrediente: " + ingrediente.getNome());
//			System.out.println("Descrição ingrediente: " + ingrediente.getDescricao());
//		}
//		
//	}
	
	
	
	
//	public static void main(String[] args) {
//		
//		//CRIAÇÃO DA BIKE
////		Quadro quadroDaBike = new Quadro("Vermelha", "Alumínio", "Grande");		
////		Roda rodaDianteiraDaBike = new Roda("20", "Alumínio", "Alumínio");		
////		Roda rodaTraseiraDaBike = new Roda("20", "Alumínio", "Alumínio");
////		Bike minhaBike = new Bike("Caloi", 0, 1, 0, 
////				quadroDaBike, rodaDianteiraDaBike, rodaTraseiraDaBike);
//		
//		Bike minhaBike = new Bike("Caloi", 0, 1, 0, 
//				new Quadro("Vermelha", "Alumínio", "Grande"), 
//				new Roda("20", "Alumínio", "Alumínio"), 
//				new Roda("20", "Alumínio", "Alumínio"));
//		
//		//A bike foi pintada de amarela
//		minhaBike.getQuadro().setCor("Amarela");
//		
//		//IMPRESSÃO DE DADOS DA BIKE
//		System.out.println("Modelo: " + minhaBike.getModelo());
//		System.out.println("Velocidade: " + minhaBike.getVelocidade());
//		System.out.println("Marcha: " + minhaBike.getMarcha());
//		System.out.println("Pedaladas: " + minhaBike.getPedaladasPorMinuto());
//		
//		System.out.println("Cor do quadro: " + minhaBike.getQuadro().getCor());
//		System.out.println("Tamanho do quadro: " + minhaBike.getQuadro().getTamanho());
//		System.out.println("Material do quadro: " + minhaBike.getQuadro().getMaterial());
//		
//		System.out.println("Cor da roda dianteira: " + minhaBike.getRodaDianteira().getCor());
//		System.out.println("Aro da roda dianteira: " + minhaBike.getRodaDianteira().getAro());
//		System.out.println("Material da roda dianteira: " + minhaBike.getRodaDianteira().getMaterial());
//		
//		
//		System.out.println("Cor da roda traseira: " + minhaBike.getRodaTraseira().getCor());
//		System.out.println("Aro da roda traseira: " + minhaBike.getRodaTraseira().getAro());
//		System.out.println("Material da roda traseira: " + minhaBike.getRodaTraseira().getMaterial());
//		
//		
//		//... daqui 20min		
//		//minhaBike.setVelocidade(20);
//		
//		//Pode isso?
//		//minhaBike.setVelocidade(-20);
//
//	}
	
	
	
//    public static void main(String[] args) {
			
			//CRIAÇÃO DA BIKE
//			Bike minhaBike = new Bike();
//			minhaBike.setModelo("Caloi");
//			minhaBike.setVelocidade(0); 
//			minhaBike.setMarcha(1);
//			minhaBike.setPedaladasPorMinuto(0);
			
//			Quadro quadroDaBike = new Quadro();
//			quadroDaBike.setCor("Vermelha");
//			quadroDaBike.setMaterial("Alumínio");
//			quadroDaBike.setTamanho("Grande");
//			
//			minhaBike.setQuadroDaBike(quadroDaBike);
//			
//			Roda rodaDianteiraDaBike = new Roda();
//			rodaDianteiraDaBike.setAro("20");
//			rodaDianteiraDaBike.setMaterial("Alumínio");
//			rodaDianteiraDaBike.setCor("Alumínio");
//			
//			minhaBike.setRodaDianteira(rodaDianteiraDaBike);
//			
//			Roda rodaTraseiraDaBike = new Roda();
//			rodaTraseiraDaBike.setAro("20");
//			rodaTraseiraDaBike.setMaterial("Alumínio");
//			rodaTraseiraDaBike.setCor("Alumínio");
//			
//			//minhaBike.setRodaTraseira(rodaTraseiraDaBike); 
//			
//			//IMPRESSÃO DE DADOS DA BIKE
//			System.out.println("Modelo: " + minhaBike.getModelo());
//			System.out.println("Velocidade: " + minhaBike.getVelocidade());
//			System.out.println("Marcha: " + minhaBike.getMarcha());
//			System.out.println("Pedaladas: " + minhaBike.getPedaladasPorMinuto());
//			
//			System.out.println("Cor do quadro: " + minhaBike.getQuadro().getCor());
//			System.out.println("Tamanho do quadro: " + minhaBike.getQuadro().getTamanho());
//			System.out.println("Material do quadro: " + minhaBike.getQuadro().getMaterial());
//			
//			System.out.println("Cor da roda dianteira: " + minhaBike.getRodaDianteira().getCor());
//			System.out.println("Aro da roda dianteira: " + minhaBike.getRodaDianteira().getAro());
//			System.out.println("Material da roda dianteira: " + minhaBike.getRodaDianteira().getMaterial());
//			
//			
//			System.out.println("Cor da roda traseira: " + minhaBike.getRodaTraseira().getCor());
//			System.out.println("Aro da roda traseira: " + minhaBike.getRodaTraseira().getAro());
//			System.out.println("Material da roda traseira: " + minhaBike.getRodaTraseira().getMaterial());
			
			
			//... daqui 20min		
			//minhaBike.setVelocidade(20);
			
			//Pode isso?
			//minhaBike.setVelocidade(-20);
//	 }

}
